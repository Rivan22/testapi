<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator as FacadesValidator;
use App\Models\CityModel;
use Illuminate\Http\Request;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $city = CityModel::all();
        return response()->json([
            "success" => true,
            "data" => $city,
            "message" => "",
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validasi tebengan
        $validator = FacadesValidator::make($request->all(), [
            'code' => 'required|unique:indonesia_cities|size:4',
            'province_code' => 'required',
            'name' => 'required',
            'meta' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json([
                "success" => false,
                "data" => null,
                "message" => $validator->errors(),
            ], 400);
        }
        // tambahkan kota baru
        $data = $request->all();
        $city = CityModel::create($data);
        return response()->json([
            "success" => true,
            "data" => $city,
            "message" => "Berhasil menambahkan kota / kabupaten baru",
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $city = CityModel::where('code', $id)->first();
        return response()->json([
            "success" => true,
            "data" => $city,
            "message" => "",
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = FacadesValidator::make($request->all(), [
            'province_code' => 'required',
            'name' => 'required',
            'meta' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json([
                "success" => false,
                "data" => null,
                "message" => $validator->errors(),
            ], 400);
        }
        // tambahkan kota baru
        $city = CityModel::where('code', $id)->first();
        if (!$city) {
            return response()->json([
                "success" => false,
                "data" => null,
                "message" => "Data kota/kabupaten tidak ditemukan",
            ], 400);
        }
        $city->province_code = $request->province_code;
        $city->name = $request->name;
        $city->meta = $request->meta;
        $city->save();
        return response()->json([
            "success" => true,
            "data" => $city,
            "message" => "Berhasil mengupdate data kota/kabupaten",
        ], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $city = CityModel::where('code', $id)->first();
        $data = [
            "success" => true,
            "data" => $city,
            "message" => "",
        ];
        if(!$city){
            $data['success'] = false;
            $data['message'] = 'Data kota/kabupaten tidak ditemukan';
            return response()->json($data, 404);
        }
        $city->delete();
        $data['message'] = 'Berhasil menghapus data tebengan';
        return response()->json($data);
    }
}
